<?php
namespace Openbravo;

/**
 * This class lets you do basic CRUD operations making HTTP requests to the Openbravo REST API.
 * See http://wiki.openbravo.com/wiki/JSON_REST_Web_Services
 *
 * @package Openbravo
 */
class BasicRESTAPIConsumer extends OpenbravoConnector
{
    /**
     * Send HTTP GET request to get data from an specific model using Openbravo REST API.
     *
     * @param string $model Name of the Openbravo model from which data will be retrieved.
     * @param int    $limit How many records to get from Openbravo model.
     * @param string $where HQL where statement that Openbravo will use to filter results.
     *
     * @return array An empty array if there was any error, or an array containing model data.
     */
    public function requestReadAll($model, $limit, $where = '')
    {
        $uri = '/openbravo/org.openbravo.service.json.jsonrest/' . $model . '/?_startRow=0&_endRow=' . $limit;
        if (!empty($where)) {
            $uri .= '&_where=' . $where;
        }
        try {
            $response = $this->client->request('GET', $uri);
            $result = json_decode($response->getBody(), true);

            // Check if there was any error
            $errors = $this->findErrors($result);
            if (empty($errors)) {
                return $result;
            } else {
                error_log($errors);
                return [];
            }
        } catch (\Exception $e) {
            $this->logException($e);
            return [];
        }
    }

    /**
     * Send HTTP GET request to get data from an specific model using Openbravo REST API.
     *
     * @param string $model Name of the Openbravo model from which data will be retrieved.
     * @param string $id    ID (primary key) of the Openbravo model to get.
     *
     * @return array An empty array if there was any error, or an array containing model data.
     */
    public function requestReadOne($model, $id)
    {
        $uri = '/openbravo/org.openbravo.service.json.jsonrest/' . $model .'/' . $id;
        try {
            $response = $this->client->request('GET', $uri);
            $result = json_decode($response->getBody(), true);

            // Check if there was any error
            $errors = $this->findErrors($result);
            if (empty($errors)) {
                return $result;
            } else {
                error_log($errors);
                return [];
            }
        } catch (\Exception $e) {
            $this->logException($e);
            return [];
        }
    }

    /**
     * Send HTTP POST request to insert data into a model using Openbravo REST API.
     *
     * @param string $model  Name of the Openbravo model where the data will be inserted.
     * @param array  $fields Name value array with the data that will be inserted on the model fields.
     *
     * @return bool True if insert inside Openbravo was successful. False otherwise.
     */
    public function requestCreate($model, $fields)
    {
        $uri = '/openbravo/org.openbravo.service.json.jsonrest/' . $model;
        try {
            $response = $this->client->request('POST', $uri, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode(['data' => $fields])
            ]);
            $result = json_decode($response->getBody(), true);

            // Check if there was any error
            $errors = $this->findErrors($result);
            if (empty($errors)) {
                return true;
            } else {
                error_log($errors);
                return false;
            }
        } catch (\Exception $e) {
            $this->logException($e);
            return false;
        }
    }

    /**
     * Send HTTP POST request to update data on a model using Openbravo REST API.
     *
     * @param string $model  Name of the Openbravo model to update with the field data.
     * @param string $id     ID (primary key) of the Openbravo model to update.
     * @param array  $fields Name value array with the data that will be updated on the model.
     *
     * @return bool True if update inside Openbravo was successful. False otherwise.
     */
    public function requestUpdate($model, $id, $fields)
    {
        $uri = '/openbravo/org.openbravo.service.json.jsonrest/' . $model;
        $fields['id'] = $id;
        try {
            $response = $this->client->request('PUT', $uri, [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode(['data' => $fields])
            ]);
            $result = json_decode($response->getBody(), true);

            // Check if there was any error
            $errors = $this->findErrors($result);
            if (empty($errors)) {
                return true;
            } else {
                error_log($errors);
                return false;
            }
        } catch (\Exception $e) {
            $this->logException($e);
            return false;
        }
    }

    /**
     * Send HTTP DELETE request to delete data a model using Openbravo REST API.
     *
     * @param string $model  Name of the Openbravo model where the data will be deleted.
     * @param string $id     ID (primary key) of the Openbravo model to delete.
     *
     * @return bool True if delete inside Openbravo was successful. False otherwise.
     */
    public function requestDelete($model, $id)
    {
        $uri = '/openbravo/org.openbravo.service.json.jsonrest/' . $model . '/' . $id;
        try {
            $response = $this->client->request('DELETE', $uri, [
                'headers' => ['Content-Type' => 'application/json']
            ]);
            $result = json_decode($response->getBody(), true);

            // Check if there was any error
            $errors = $this->findErrors($result);
            if (empty($errors)) {
                return true;
            } else {
                error_log($errors);
                return false;
            }
        } catch (\Exception $e) {
            $this->logException($e);
            return false;
        }
    }

    /**
     * Print an exception details inside PHP error log
     *
     * @param \Exception $exception
     */
    private function logException($exception)
    {
        error_log('Error interno al conectarse a Openbravo.' .
            '[' . $exception->getCode() . ']' . $exception->getMessage() .
            '. En la línea ' . $exception->getLine() .
            ' del archivo' . $exception->getFile());
    }
}