<?php
namespace Openbravo;

use GuzzleHttp\Client;

/**
 * This is the base class that constructs the Guzzle HTTP Client to
 * send HTTP requests (GET, POST, PUT, DELETE) to the Openbravo REST API.
 *
 * @package Openbravo
 */
class OpenbravoConnector
{
    protected $client;
    private   $host;
    private   $port;
    private   $username;
    private   $password;

    /**
     * OpenbravoConnector constructor.
     *
     * @param string $host     IP or domain where Openbravo is running
     * @param string $port     TCP port where Openbravo is listening for REST API connections
     * @param string $username
     * @param string $password
     */
    public function __construct($host, $port, $username, $password)
    {
        $this->host     = $host;
        $this->port     = $port;
        $this->username = $username;
        $this->password = $password;
        $this->client   = $this->setupOpenbravoRestClient();
    }

    /**
     * Setups a Guzzle HTTP Client to send HTTP Requests to Openbravo REST API
     *
     * @return Client
     */
    protected function setupOpenbravoRestClient()
    {
        $fullURL = $this->host . ':' . $this->port;
        $authData = [
            $this->username,
            $this->password
        ];
        return new Client([
            'base_uri'    => $fullURL,
            'timeout'     => 30,
            'auth'        => $authData,
            'http_errors' => false
        ]);
    }

    /**
     * Finds errors inside the data from the HTTP Response
     *
     * @param array $httpResponse JSON decoded array that contains the Openbravo HTTP response.
     *
     * @return string Text that describes the errors inside Openbravo caused for the API call.
     */
    protected function findErrors($httpResponse)
    {
        if (intval($httpResponse['response']['status']) !== 0) {
            $error = '';
            if (isset($httpResponse['response']['error']['message'])) {
                $error = $httpResponse['response']['error']['message'];
            } else {
                if(isset($httpResponse['response']['errors'])){
                    foreach ($httpResponse['response']['errors'] as $property => $value) {
                        $error .= $property . ': ' . $value;
                    }
                }
            }
            return $error;
        }
        return '';
    }

    /**
     * Gets a Client object that lets you
     * make HTTP Requests to the Openbravo API.
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
