<?php
require "../vendor/autoload.php";
require "config.php";
use Openbravo\BasicRESTAPIConsumer;

$consumer = new BasicRESTAPIConsumer(OB_HOST, OB_PORT, OB_USER, OB_PASSWORD);

$result = $consumer->requestReadAll('BusinessPartner', 10);
if (!empty($result)) {
    if (isset($_GET['json']) && boolval($_GET['json']) === true) {
        header('Content-Type: application/json');
        echo json_encode($result);
    } else { ?>
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Ejemplo Lectura de Terceros</title>
        </head>
        <body>
        <h1>Terceros</h1>
        <table>
            <thead>
            <tr>
                <th>Identificador</th>
                <th>Nombre</th>
                <th>Categoría</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($result['response']['data'] as $businessPartner) { ?>
                <tr>
                    <td><?php echo $businessPartner['searchKey']; ?></td>
                    <td><?php echo $businessPartner['name']; ?></td>
                    <td><?php echo $businessPartner['businessPartnerCategory$_identifier']; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </body>
        </html>
<?php
    }
}
