<?php
require "../vendor/autoload.php";
require "config.php";
use Openbravo\BasicRESTAPIConsumer;

$consumer = new BasicRESTAPIConsumer(OB_HOST, OB_PORT, OB_USER, OB_PASSWORD);

$categories = [];
$result = $consumer->requestReadAll('BusinessPartnerCategory', 10);
foreach ($result['response']['data'] as $businessPartnerCategory) {
    $categories[] = $businessPartnerCategory;
}

$isPOST = false;
$isSuccess = false;
if (isset($_POST['name']) && isset($_POST['searchKey'])) {
    $isPOST = true;
    $bpResult = $consumer->requestReadAll('BusinessPartner', 1, "searchKey='{$_POST['searchKey']}'");
    if (isset($bpResult['response']['data'][0]['id'])) {
        $businessPartnerID = $bpResult['response']['data'][0]['id'];
        $fields = [
            'name'      => $_POST['name'],
            'searchKey' => $_POST['searchKey'],
            'businessPartnerCategory' => $_POST['businessPartnerCategory']
        ];
        $isSuccess = $consumer->requestUpdate('BusinessPartner', $businessPartnerID, $fields);
    } else {
        error_log('No existe un tercero con el identificador proporcionado.');
    }
} ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ejemplo Actualización de Terceros</title>
</head>
<body>
<h1>Actualizar tercero</h1>
<?php if ($isPOST) { ?>
    <p>
        <?php if ($isSuccess) { ?>
            <strong>Tercero actualizado, buscar en ventana en Openbravo con el identificador del tercero.</strong>
        <?php } else { ?>
            <strong>Actualización fallida, ver log de errores de PHP.</strong>
        <?php } ?>
    </p>
<?php } else { ?>
    <form action="" method="post">
        <label for="fieldName">Nombre</label>
        <input id="fieldName" type="text" name="name">
        <label for="fieldSearchKey">Identificador</label>
        <input id="fieldSearchKey" type="text" name="searchKey">
        <label for="fieldBusinessPartnerCategory">Categoría</label>
        <select id="fieldBusinessPartnerCategory" name="businessPartnerCategory">
            <?php foreach ($categories as $category) { ?>
                <option value="<?php echo $category['id'] ?>">
                    <?php echo $category['name'] ?>
                </option>
            <?php } ?>
        </select>
        <input type="submit" value="Actualizar tercero">
    </form>
<?php } ?>
</html>