<?php
require "../vendor/autoload.php";
require "config.php";
use Openbravo\BasicRESTAPIConsumer;

$consumer = new BasicRESTAPIConsumer(OB_HOST, OB_PORT, OB_USER, OB_PASSWORD);
$categories = [];
$result = $consumer->requestReadAll('BusinessPartnerCategory', 10);
foreach ($result['response']['data'] as $businessPartnerCategory) {
    $categories[] = $businessPartnerCategory;
}

$isPOST = false;
$isSuccess = false;
if (isset($_POST['name']) && isset($_POST['searchKey'])) {
    $isPOST = true;
    $fields = [
        'name'      => $_POST['name'],
        'searchKey' => $_POST['searchKey'],
        'businessPartnerCategory' => $_POST['businessPartnerCategory']
    ];
    $isSuccess = $consumer->requestCreate('BusinessPartner', $fields);
} ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Ejemplo Creación de Terceros</title>
</head>
<body>
<h1>Crear tercero</h1>
<?php if ($isPOST && $isSuccess) { ?>
    <p>
        <strong>Tercero creado, buscar en ventana de Openbravo con el identificador proporcionado.</strong>
    </p>
<?php } else { ?>
    <form action="" method="post">
        <label for="fieldName">Nombre</label>
        <input id="fieldName" type="text" name="name">
        <label for="fieldSearchKey">Identificador</label>
        <input id="fieldSearchKey" type="text" name="searchKey">
        <label for="fieldBusinessPartnerCategory">Categoría</label>
        <select id="fieldBusinessPartnerCategory" name="businessPartnerCategory">
            <?php foreach ($categories as $category) { ?>
                <option value="<?php echo $category['id'] ?>">
                    <?php echo $category['name'] ?>
                </option>
            <?php } ?>
        </select>
        <input type="submit" value="Crear tercero">
    </form>
<?php } ?>
</html>