<?php
require "../vendor/autoload.php";
require "config.php";
use Openbravo\BasicRESTAPIConsumer;

$consumer = new BasicRESTAPIConsumer(OB_HOST, OB_PORT, OB_USER, OB_PASSWORD);

if (isset($_GET['id']) && ctype_alnum($_GET['id'])) {
    $isSuccess = $consumer->requestDelete('BusinessPartner', $_GET['id']);
    if ($isSuccess) {
        echo 'Tercero eliminado.';
    } else {
        echo 'Error al eliminar tercero. Ver el log de errores de PHP.';
    }
} else {
    echo 'Debes pasar un ID de tercero válido en la variable GET "id"';
}
