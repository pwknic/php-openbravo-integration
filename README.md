##README

Se deb� usar composer para descargar las dependencias del proyecto.
Con el siguiente comando se instalan dichas dependencias.

    php composer.phar install

Acceder a la carpeta public para ver los ejemplos.

**�Importante!**

Se debe cambiar el host, port, user y password a los correspondientes
a tu entorno de Openbravo para que los ejemplos se conecten correctamente.
Estas variables globales se encuentran en el archivo public/config.php

    define('OB_HOST', 'localhost');
    define('OB_PORT', '8080');
    define('OB_USER', 'Openbravo');
    define('OB_PASSWORD', 'openbravo');
